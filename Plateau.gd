extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var positionCartes = [Vector2(20,550), Vector2(320, 550), Vector2(620, 550), Vector2(920, 550), Vector2(1220, 550)]

func _ready():
	affiche_main()
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Sacrifice_button_down():
	for carte in get_tree().get_nodes_in_group("Main"):
		carte.sacrifice = not carte.sacrifice

func affiche_main():
	var i = 0
	for carte in get_tree().get_nodes_in_group("Deck")[0].main:
		carte.add_to_group("Main")
		carte.rect_position = positionCartes[i]
		carte.visible = true
		i += 1