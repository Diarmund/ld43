extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var nombreCarte = 5
var deck = []
var main = []
var defausse = []

func _ready():
	deck = get_children()
	randomize()
	deck = shuffleList(deck)
	while len(main) < nombreCarte and len(deck) != 0:
		pioche()




func shuffleList(list):
    var shuffledList = [] 
    var indexList = range(list.size())
    for i in range(list.size()):
        var x = randi()%indexList.size()
        shuffledList.append(list[indexList[x]])
        indexList.remove(x)
    return shuffledList


func pioche():
	main += [deck[0]]
	deck[0].connect("played", self, "_on_Carte_played")
	deck[0].connect("sacrificied", self, "_on_Carte_sacrificied")
	deck.remove(0)
	


func _on_Carte_played(carte):
	var n = main.find(carte)
	main.remove(n)
	defausse += [carte]
	carte.visible = false
	print(defausse)

func _on_Carte_sacrificied(carte):
	var n = main.find(carte)
	main.remove(n)
	remove_and_skip(carte)

