extends Button

var carte_ID = 0
var sacrifice = false
signal played
signal sacrificied
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready(event):
	pass


#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_play():
	print("Je suis jouée : 0")
	emit_signal("played", self)

func _on_sacrifice():
	print("Je suis sacrifié : 0")
	emit_signal("sacrificied", self)




# replace with function body


func _on_Carte_pressed():
	if sacrifice:
		_on_sacrifice()
	else:
		_on_play()
